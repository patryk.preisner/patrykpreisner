import { Component, OnInit } from '@angular/core';
import linkedinFill from '@iconify-icons/akar-icons/linkedin-fill';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  linkedInIcon = linkedinFill
  constructor() { }

  ngOnInit(): void {
  }

}
