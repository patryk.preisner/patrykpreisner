import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from  './home/home.component';
import {PrivateProjectsComponent} from './private-projects/private-projects.component';
import {StudentResearchProjectsComponent} from './student-research-projects/student-research-projects.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import {CvComponent} from './cv/cv.component'

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent},
  { path: 'private_projects', component: PrivateProjectsComponent} ,
  { path: 'student_projects', component: StudentResearchProjectsComponent} ,
  {path: 'cv', component: CvComponent},
  { path: '404', component: PageNotFoundComponent },  
  { path: '**', redirectTo: '404' }, 

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
