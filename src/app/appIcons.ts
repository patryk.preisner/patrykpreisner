import linkedinFill from '@iconify-icons/akar-icons/linkedin-fill';
import xingIcon from '@iconify-icons/icomoon-free/xing';
import gitlabIcon from '@iconify-icons/cib/gitlab';


export const appIcons = {
  "LinkedIn": linkedinFill,
  "Xing": xingIcon,
  "GitLab": gitlabIcon
}