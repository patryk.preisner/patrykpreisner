import { Component, OnInit } from '@angular/core';
import {Post} from '../../models/post'

@Component({
  selector: 'app-student-research-projects',
  templateUrl: './student-research-projects.component.html',
  styleUrls: ['./student-research-projects.component.scss']
})
export class StudentResearchProjectsComponent implements OnInit {
  posts:Post[] = []
  constructor() { }

  ngOnInit(): void {
    this.posts.push(
      new Post(
        'The Intelligent Enterprise',
        '02-05-2022',
        'Student project',
        'lorem ipsum.....'
        )
    )
  }
  
  

}
