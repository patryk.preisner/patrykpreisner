import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BachelorThesisComponent } from './bachelor-thesis.component';

describe('BachelorThesisComponent', () => {
  let component: BachelorThesisComponent;
  let fixture: ComponentFixture<BachelorThesisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BachelorThesisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BachelorThesisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
