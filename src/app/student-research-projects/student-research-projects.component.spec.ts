import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentResearchProjectsComponent } from './student-research-projects.component';

describe('StudentResearchProjectsComponent', () => {
  let component: StudentResearchProjectsComponent;
  let fixture: ComponentFixture<StudentResearchProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentResearchProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentResearchProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
