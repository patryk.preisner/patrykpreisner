import { Component } from '@angular/core';
import { IconService } from '@visurel/iconify-angular';
import { appIcons } from './appIcons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'patrykpreisner';
  constructor(iconService: IconService){
    iconService.registerAll(appIcons);
  }
}
