import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import { FooterComponent } from './footer/footer.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatChipsModule} from '@angular/material/chips';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import { IconModule } from '@visurel/iconify-angular';
import { PrivateProjectsComponent } from './private-projects/private-projects.component';
import { StudentResearchProjectsComponent } from './student-research-projects/student-research-projects.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { BachelorThesisComponent } from './student-research-projects/bachelor-thesis/bachelor-thesis.component';
import { CvComponent } from './cv/cv.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    PrivateProjectsComponent,
    StudentResearchProjectsComponent,
    PageNotFoundComponent,
    BachelorThesisComponent,
    CvComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    FlexLayoutModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    MatChipsModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    IconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
