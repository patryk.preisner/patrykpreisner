import { throwToolbarMixedModesError } from "@angular/material/toolbar"

export class Post{
    title:string
    date:string
    abridged:string
    fulltext:string
    constructor(title:string,date:string,abridged:string,fulltext:string){
        this.title = title
        this.date=date
        this.abridged = abridged
        this.fulltext = fulltext
    }
}